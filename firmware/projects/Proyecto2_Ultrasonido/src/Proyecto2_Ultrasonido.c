/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto2_Ultrasonido.h"
#include "../inc/Proyecto2_Ultrasonido.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "delay.h"
#include "switch.h"
#include "hc_sr4.h"
#include "DisplayITS_E0803.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

bool in_cm = false; // 0  si en cm y 1 si en in
bool on_off = true;
bool display = false;

/*==================[internal functions declaration]=========================*/

/** @brief Función llamada por interrupción de tecla 1.
 * Modifica la variable booleana on_off.
 */
void Tecla1();

/** @brief Función llamada por interrupción de tecla 2.
 * Modifica la variable booleana display.
 */
void Tecla2();

/** @brief Función llamada por interrupción de tecla 3.
 * Fija la medida en cm.
 */
void Tecla3();

/** @brief Función llamada por interrupción de tecla 4.
 * Fija la medida en pulgadas.
 */
void Tecla4();


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();

	HcSr04Init(T_FIL2,T_FIL3);

	gpio_t pines[7] = {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5};
	ITSE0803Init(pines);

	int16_t distancia=0;

	SwitchActivInt(SWITCH_1, Tecla1);
	SwitchActivInt(SWITCH_2, Tecla2);
	SwitchActivInt(SWITCH_3, Tecla3);
	SwitchActivInt(SWITCH_4, Tecla4);

    while(1)
    {
    	if(on_off == true)
    	{
    		if(in_cm == false)
    			distancia = HcSr04ReadDistanceCentimeters();
    		if(in_cm == true)
    			distancia = HcSr04ReadDistanceInches();
    	}

    	if(on_off == false)
    		distancia = 0;

    	if(display == false)
    	ITSE0803DisplayValue(distancia);

    	DelayMs(500);
	}
    
	return 0;
}


void Tecla1()
{
	on_off = !on_off;
}

void Tecla2()
{
	display = !display;
}

void Tecla3()
{
	in_cm = false;
}

void Tecla4()
{
	in_cm = true;
}



/*==================[end of file]============================================*/


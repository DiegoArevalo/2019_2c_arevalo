/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 *		Diego Arévalo
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/parcial.h"
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "switch.h"
#include "DisplayITS_E0803.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

//Buffer para acumular datos durante un segundo
#define WINDOW_WIDTH 250 //250 Hz, 250 datos por segundo

//Período de muestreo
#define SAMPLE_FREC_US (1000000/250) //250 Hz

//Booleano utilizado como bandera para procesar datos y empezar un nuevo buffer
bool new_window = false;

//Vector que contiene la señal
uint16_t senial[WINDOW_WIDTH];

//Variable auxiliar que contiene el dato a guardar en el vector señal
uint16_t aux_senial;

//Contador asociado a la posición del vector señal
uint16_t contador_senial = 0;

//Variable que contiene el máximo
uint16_t max = 0;

//Variable auxiliar para obtener el máximo
uint16_t aux_max = 0;

//Variable que contiene el mínimo
uint16_t min = 0;

//Variable auxiliar para obtener el mínimo
uint16_t aux_min;

//Variable que contiene el promedio
uint16_t promedio = 0;

//Variable auxiliar para acumular los valores y luego calcular el promedio
uint32_t acumulador = 0;

//Defines para mostrar max, min o promedio
#define SHOW_MAX 0
#define SHOW_MIN 1
#define SHOW_PROM 2

//Variable que contiene el valor que dirá qué será mostrado en el display
uint8_t valor_display = SHOW_MAX;

//Bandera que activa la muestra luego del primer segundo de medición
bool muestra_obtenida = false;


/*==================[internal functions declaration]=========================*/

/** @brief Función llamada por interrupción de tecla 1.
 * Muestra máximo.
 */
void Tecla1();

/** @brief Función llamada por interrupción de tecla 2.
 * Muestra mínimo.
 */
void Tecla2();

/** @brief Función llamada por interrupción de tecla 3.
 * Muestra promedio.
 */
void Tecla3();

/** @brief Función llamada por timer.
 * Activa conversión AD.
 */
void TempInt();

/** @brief Función llamada por interrupción conversor AD.
 * Lee dato convertido
 */
void LecturaAD();

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	LedsInit();

	//Para inicializar el display, creo un vector con los pines a los que será conectado
	gpio_t pines[7] = {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5};
	ITSE0803Init(pines);

	//Fijamos configuración para inicializar conversor AD
	analog_input_config ad_config;
	ad_config.input = CH1; //Canal 1
	ad_config.mode = AINPUTS_SINGLE_READ;
	ad_config.pAnalogInput = LecturaAD;

	AnalogInputInit(&ad_config);


	//Estructura para inicializar uart
	serial_config puerto_serie;
	puerto_serie.port = SERIAL_PORT_PC;	//Uso puerto PC
	puerto_serie.baud_rate = 115200; //bits por segundo
	puerto_serie.pSerial = NO_INT; //No uso una función porque sólo envío datos

	UartInit(&puerto_serie);


	//Estructura para inicializar timer
	timer_config temporizador;
	temporizador.period = SAMPLE_FREC_US; //Período de muestreo
	temporizador.timer = TIMER_A; //Uso SysTick
	temporizador.pFunc = TempInt;
	TimerInit(&temporizador);

	TimerStart(TIMER_A);


	//Interrupciones de teclas
	SwitchActivInt(SWITCH_1, Tecla1);
	SwitchActivInt(SWITCH_2, Tecla2);
	SwitchActivInt(SWITCH_3, Tecla3);


	while(1)
	{
		if(muestra_obtenida)
		{
			//Apago los LEDs
			LedsOffAll();

			if(new_window)
			{

				//Prende distintos leds según en qué rango de valores se encuentre el máximo
				//Mayor a 150 -> rojo
				if (max > 150)
					LedOn(LED_RGB_R);
				//Entre 150 y 50 -> azul
				else if (max < 150 && max > 50)
					LedOn(LED_RGB_B);
				//Menor a 50 -> verde
				else if (max < 50)
					LedOn(LED_RGB_G);
				else
					LedsOffAll();

				new_window = false;
			}


			switch(valor_display)
			{
			//Máximo
			case SHOW_MAX:
				//Muestra
				ITSE0803DisplayValue(max);
				//Prende LED rojo
				LedOn(LED_1);

				//Envío de datos por puerto serie

				//String que especifica qué es el dato
				UartSendString(puerto_serie.port, " Máximo: ");
				//Valor pasado como dígitos ASCII
				UartSendString(puerto_serie.port, UartItoa(max,10));
				//Caracter espacio, unidad y cambio de línea
				UartSendString(puerto_serie.port, " mmHg \r\n");

				break;

			//Mínimo
			case SHOW_MIN:
				//Muestra
				ITSE0803DisplayValue(min);
				//Prende LED amarillo
				LedOn(LED_2);

				//Envío de datos por puerto serie

				//String que especifica qué es el dato
				UartSendString(puerto_serie.port, " Mínimo: ");
				//Distancia pasada como dígitos ASCII
				UartSendString(puerto_serie.port, UartItoa(min,10));
				//Caracter espacio, unidad y cambio de línea
				UartSendString(puerto_serie.port, " mmHg\r\n");

				break;

			//Promedio
			case SHOW_PROM:
				//Muestra
				ITSE0803DisplayValue(promedio);
				//Prende LED verde
				LedOn(LED_3);

				//Envío de datos por puerto serie

				//String que especifica qué es el dato
				UartSendString(puerto_serie.port, " Promedio: ");
				//Distancia pasada como dígitos ASCII
				UartSendString(puerto_serie.port, UartItoa(promedio,10));
				//Caracter espacio, unidad y cambio de línea
				UartSendString(puerto_serie.port, " mmHg\r\n");

				break;
			}
		}
	}
	return 0;
}


//Función de la interrupción de la tecla 1
void Tecla1()
{
	valor_display = SHOW_MAX;
}

//Función de la interrupción de la tecla 2
void Tecla2()
{
	valor_display = SHOW_MIN;
}

//Función de la interrupción de la tecla 3
void Tecla3()
{
	valor_display = SHOW_PROM;
}



//Función del temporizador
void TempInt()
{
	//Función que comienza a convertir el dato AD, una vez que termina de convertir, llama a la función LecturaAD()
	AnalogStartConvertion();
}



void LecturaAD()
{
	//Lee dato y lo guarda en variable auxiliar
	AnalogInputRead(CH1, &aux_senial);

	//Asigna el valor de la variable auxiliar al componente del vector correspondiente
	if(contador_senial < WINDOW_WIDTH)
	{
		senial[contador_senial] = aux_senial*(3.3/200); //Valor recibido, teniendo en cuenta que para 3.3V, la presión es 200mmHg

		//Aumenta el valor del contador en uno
		contador_senial++;

		//Cálculo de máximo
		if(aux_max < aux_senial)
			aux_max = aux_senial;

		//Cálculo de mínimo
		if(aux_min > aux_senial)
			aux_min = aux_senial;

		//Acumulo para calcular el promedio
		acumulador += aux_senial;
	}
	//Cuando el contador llega al máximo, se reinicia
	else
	{
		//Le asigno los valores correspondientes a máximo, mínimo y promedio
		max = aux_max;
		min = aux_min;
		promedio = acumulador/(contador_senial+1);

		//Reinicio el valor de las variables
		aux_max = 0;
		aux_min = 0;
		acumulador = 0;
		contador_senial = 0;

		//Tengo un segundo de datos, pasa a true new_window y muestra_obtenida
		new_window = true;
		muestra_obtenida = true;

	}
}

/*==================[end of file]============================================*/



﻿Parcial

Se implementa un sistema de adquisición que digitaliza y procesa datos de un sensor de presión.
Calcula el valor máximo, mínimo y el promedio del último segundo de medición.
Teniendo en cuenta el valor máximo, el color del LED RGB cambiará, siendo el rojo para presiones altas (mayores a 150 mmHg), el azul para las intermedias
(de 50 mmHg a 150mmHg) y el verde para las bajas (menores a 50 mmHg).
El sistema mostrará por un display un valor, el cual también será enviado a la PC por un puerto serie.
El valor mostrado será el máximo cuando se presione la tecla 1, el mínimo cuando se presione la tecla 2 y el promedio cuando se presione la tecla 3.
Según qué se muestre, se encenderá un LED, si es el máximo, se enciende el rojo, si es el mínimo, se enciende el amarillo, y si es el promedio, se enciende el verde.

Para implementar la aplicación, se requiere la conexión de dos dispositivos:

	Un sensor de presión
	Un display (ITS E0803)

El sensor se conecta:
	+V a 3V3
	GND a GNDA
	OUT a CH1 (P1,13)

El display se conecta:
	+V a 5V(P2,2)
	GND a GND(P2)
	El resto de los pines van conectados sobre P2: LCD1(P2,30), LCD2(P2,28), LCD3(P2,26), LCD4(P2,22), GPIO1(P2,32), GPIO3(P2,34), GPIO5(P2,36), etc.
	
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto3_LecturaComandos.h"
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"
#include "switch.h"
#include "hc_sr4.h"
#include "DisplayITS_E0803.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

bool in_cm = false; // 0  si en cm y 1 si en in
bool on_off = true;
bool display = false;
bool timer_on_off = true;
int16_t distancia=0;

/*==================[internal functions declaration]=========================*/

/** @brief Función llamada por interrupción de tecla 1.
 * Modifica la variable booleana on_off.
 */
void Tecla1();

/** @brief Función llamada por interrupción de tecla 2.
 * Modifica la variable booleana display.
 */
void Tecla2();

/** @brief Función llamada por interrupción de tecla 3.
 * Fija la medida en cm.
 */
void Tecla3();

/** @brief Función llamada por interrupción de tecla 4.
 * Fija la medida en pulgadas.
 */
void Tecla4();

/** @brief Función llamada por timer.
 * Activa la medición.
 */
void TempInt();

/** @brief Función llamada por uart.
 * Swichea entre 4 opciones.
 */
void LecturaPC();


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();

	HcSr04Init(T_FIL2,T_FIL3);

	gpio_t pines[7] = {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5};
	ITSE0803Init(pines);


	//Estructura para inicializar uart
	serial_config puerto_serie;
	puerto_serie.port = SERIAL_PORT_PC;	//Usamos puerto PC
	puerto_serie.baud_rate = 115200; //bits por segundo
	puerto_serie.pSerial = LecturaPC; //No usamos una función porque sólo enviamos datos

	UartInit(&puerto_serie);


	//Estructura para inicializar timer
	timer_config temporizador;
	temporizador.period = 1000;
	temporizador.timer = TIMER_B;
	temporizador.pFunc = TempInt;
	TimerInit(&temporizador);

	TimerStart(TIMER_B);



	//Interrupciones de teclas
	SwitchActivInt(SWITCH_1, Tecla1);
	SwitchActivInt(SWITCH_2, Tecla2);
	SwitchActivInt(SWITCH_3, Tecla3);
	SwitchActivInt(SWITCH_4, Tecla4);


    while(1)
    {
    	if(on_off == true)
    	{
    		//Se asigna unidad en la que se mide distancia
    		if(in_cm == false)
    		{
    			distancia = HcSr04ReadDistanceCentimeters();

    			//Envío de datos por puerto serie

    			//Distancia pasada como dígitos ASCII
    			UartSendString(puerto_serie.port, UartItoa(distancia,10));
    			//Caracter espacio, unidad y cambio de línea
    			UartSendString(puerto_serie.port, " cm\r\n");

    		}
    		if(in_cm == true)
    		{
    			distancia = HcSr04ReadDistanceInches();

    			//Envío de datos por puerto serie

    			//Distancia pasada como dígitos ASCII
    			UartSendString(puerto_serie.port, UartItoa(distancia,10));
    			//Caracter espacio, unidad y cambio de línea
    			UartSendString(puerto_serie.port, " in\r\n");
    		}



    		on_off = false;
    	}

    	if(display == false)
    	ITSE0803DisplayValue(distancia);

	}
    
	return 0;
}


void Tecla1()
{
	if(timer_on_off == false)
	TimerStart(TIMER_B);

	if(timer_on_off == true)
	{
		TimerStop(TIMER_B);
		distancia = 0;
	}

	timer_on_off = !timer_on_off;
}

void Tecla2()
{
	display = !display;
}

void Tecla3()
{
	in_cm = false;
}

void Tecla4()
{
	in_cm = true;
}

void TempInt()
{
	on_off = true;
}

void LecturaPC()
{
	uint8_t data;
	UartReadByte(SERIAL_PORT_PC,&data);
	switch(data)
	{
	case 'a': //Cumple la función de la tecla 1
		if(timer_on_off == false)
			TimerStart(TIMER_B);

		if(timer_on_off == true)
		{
			TimerStop(TIMER_B);
			distancia = 0;
		}

		timer_on_off = !timer_on_off;

		break;

	case 's': //Cumple la función de la tecla 2
		display = !display;
		break;

	case 'd': //Cumple la función de la tecla 3
		in_cm = false;
		break;

	case 'f': //Cumple la función de la tecla 4
		in_cm = true;
		break;
	}
}

/*==================[end of file]============================================*/



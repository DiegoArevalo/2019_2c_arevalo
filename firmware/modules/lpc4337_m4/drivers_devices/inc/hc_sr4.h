/*
 * hc_sr4.h
 *
 *  Created on: 29 ago. 2019
 *      Author: Usuario
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_HC_SR4_H_
#define MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_HC_SR4_H_

/*****************************************************************************
 * Public macros/types/enumerations/variables definitions
 ****************************************************************************/


/*==================[inclusions]=============================================*/
#include "gpio.h"
#include "stdint.h"
#include "bool.h"





/*****************************************************************************
 * Public functions definitions
 ****************************************************************************/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

//		INICIALIZA EL HCSR4
bool HcSr04Init(gpio_t echo, gpio_t trigger);

//		DEVUELVE LA DISTANCIA EN CENTIMETROS
int16_t HcSr04ReadDistanceInches(void);

//		DEVUELVE LA DISTANCIA EN PULGADAS
int16_t HcSr04ReadDistanceCentimeters(void);

//		DESINICIALIZA EL HCSR4
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);






#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_HC_SR4_H_ */

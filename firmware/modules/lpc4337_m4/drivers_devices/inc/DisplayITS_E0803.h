/* @brief  EDU-CIAA NXP GPIO driver
 *
 *
 * This driver provide functions to generate delays using Timer0
 * of LPC4337
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/11/2018 | Document creation		                         |
 *
 */

#ifndef DISPLAYITS_E0803_H_
#define DISPLAYITS_E0803_H_

#include <stdint.h>
#include "gpio.h"

/*****************************************************************************
 * Public macros/types/enumerations/variables definitions
 ****************************************************************************/

/*****************************************************************************
 * Public functions definitions
 ****************************************************************************/

/**
 * @brief Init driver
 * @param[in] gpio *pins
 * @return TRUE if no error
 */
bool ITSE0803Init(gpio_t * pins);

/**
 * @brief assign value
 * @param[in] uint16_t valor
 * @return TRUE if no error
 */
bool ITSE0803DisplayValue(uint16_t valor);

/**
 * @brief read value
 * @param[in] no parameters
 * @return uint16_t value
 */
uint16_t ITSE0803ReadValue(void);

/**
 * @brief Deinit driver
 * @param[in] gpio *pins
 * @return TRUE if no error
 */
bool ITSE0803Deinit(gpio_t * pins);


#endif /* DISPLAYITS_E0803_H_ */

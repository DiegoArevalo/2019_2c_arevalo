/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/


#define ON 1
#define OFF 0
#define TOGGLE 2

//Creo struct leds
typedef struct {
		uint8_t n_led; //1, 2, 3
		uint8_t n_ciclos;
		uint8_t periodo;
		uint8_t mode; //ON = 1, OFF = 0, TOGGLE = 2
	} leds;

void ManejoLeds (leds *led);

int main(void)
{
	leds leds_prueba;
	leds_prueba.n_led = 2;
	leds_prueba.n_ciclos = 3;
	leds_prueba.periodo = 5;
	leds_prueba.mode = TOGGLE;
	leds *pl = &leds_prueba;

	ManejoLeds (pl);



	return 0;
}


void ManejoLeds (leds *led) {
	if (led->mode == ON){
		if (led->n_led == 1)
			printf("LED 1 encendido \r\n");
		else if (led->n_led == 2)
					printf("LED 2 encendido \r\n");
		else if (led->n_led == 3)
					printf("LED 3 encendido \r\n");
	}

	else if (led->mode == OFF){
		if (led->n_led == 1)
					printf("LED 1 apagado \r\n");
		else if (led->n_led == 2)
					printf("LED 2 apagado \r\n");
		else if (led->n_led == 3)
					printf("LED 3 apagado \r\n");
	}

	else if (led->mode == TOGGLE){
		int i;
		for (i=0; i<led->n_ciclos; ++i){
			if (led->n_led == 1)
				printf("LED 1 toggled \r\n");
			else if (led->n_led == 2)
					printf("LED 2 toggled \r\n");
			else if (led->n_led == 3)
					printf("LED 3 toggled \r\n");

			int j;
			for(j=0; j<led->periodo; ++j)
			{
				printf("\r\n %d \r\n",j+1);
			}
		}
	}
		printf("FIN");
}



/*==================[end of file]============================================*/


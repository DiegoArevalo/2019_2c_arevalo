########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

#PROYECTO_ACTIVO = ejercicio_9
#NOMBRE_EJECUTABLE = ejercicio_9.exe

#PROYECTO_ACTIVO = ejercicio_12
#NOMBRE_EJECUTABLE = ejercicio_12.exe

#PROYECTO_ACTIVO = ejercicio_14
#NOMBRE_EJECUTABLE = ejercicio_14.exe

#PROYECTO_ACTIVO = ejercicio_16
#NOMBRE_EJECUTABLE = ejercicio_16.exe

#PROYECTO_ACTIVO = ejercicio_integrador_A
#NOMBRE_EJECUTABLE = ejercicio_integrador_A

#PROYECTO_ACTIVO = ejercicio_integrador_B
#NOMBRE_EJECUTABLE = ejercicio_integrador_B

#PROYECTO_ACTIVO = ejercicio_integrador_C
#NOMBRE_EJECUTABLE = ejercicio_integrador_C

PROYECTO_ACTIVO = ejercicio_integrador_D
NOMBRE_EJECUTABLE = ejercicio_integrador_D
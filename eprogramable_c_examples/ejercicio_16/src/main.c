/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*Declare una variable sin signo de 32 bits y cargue el valor 0x01020304. Declare cuatro variables sin signo de 8 bits y,
 * utilizando máscaras, rotaciones y truncamiento, cargue cada uno de los bytes de la variable de 32 bits.
Realice el mismo ejercicio, utilizando la definición de una “union”.
 */


int main(void)
{
	//Creo variables
	uint32_t A = 0x01020304;
	uint8_t a,b,c,d;

	//Asigno bytes
	a = A;
	b = (A >> 8);
	c = (A >> 16);
	d = (A >> 24);

	printf ("%d \r\n %d \r\n %d \r\n %d \r\n",a,b,c,d);

	//Creo unión
	union bytes {
		struct cada_byte{
		uint8_t byte_1;
		uint8_t byte_2;
		uint8_t byte_3;
		uint8_t byte_4;
	}bytes_8;
		uint32_t numero;
	} union_bytes;

	union_bytes.numero = A;

	printf ("%d \r\n %d \r\n %d \r\n %d \r\n",union_bytes.bytes_8.byte_1,union_bytes.bytes_8.byte_2,union_bytes.bytes_8.byte_3,union_bytes.bytes_8.byte_4);

	return 0;
}

/*==================[end of file]============================================*/


/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/

#define IN 0
#define OUT 1

/*==================[internal functions declaration]=========================*/

//Defino struct
typedef struct
{
    uint8_t port;                /*!< GPIO port number */
    uint8_t pin;                /*!< GPIO pin number */
    uint8_t dir;                /*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;


void ColocarBits(gpioConf_t * vec, uint8_t bcd);


int i;


int main(void)
{

	//Creo struct
	gpioConf_t bits[4] =
	{
		{1, 4, OUT},
		{1, 5, OUT},
		{1, 6, OUT},
		{2, 14, OUT},
	};

	//Creo número BCD
	uint8_t num_bcd = 3;

	ColocarBits(bits, num_bcd);

	return 0;
}


void ColocarBits(gpioConf_t * vec, uint8_t bcd)
{
	uint8_t mask_bit [4]= {1, 1<<1, 1<<2, 1<<3};
	uint8_t vec_aux [4];

	for(i = 0; i < 4; ++i)
	{
		if((mask_bit[i] & bcd) == 0)
				vec_aux[i] = 0;
		else vec_aux[i] = 1;

		printf("Puerto %d.%d en %d \n \r", vec[i].port, vec[i].pin, vec_aux[i]);
	}
}


/*==================[end of file]============================================*/


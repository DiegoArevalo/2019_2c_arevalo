/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20 caracteres y edad.
Defina una variable con esa estructura y cargue los campos con sus propios datos.
Defina un puntero a esa estructura y cargue los campos con los datos de su compañero (usando acceso por punteros).
 */


int main(void)
{
	//Creo struct alumno
	typedef struct {
		char nombre [12];
		char apellido [20];
		uint8_t edad;
	} Alumno;

	//Creo variable alumno1 (yo)
	Alumno alumno1;
	strcpy(alumno1.nombre, "Diego");
	strcpy(alumno1.apellido, "Arevalo");
	alumno1.edad = 21;

	printf("%s \r\n %s \r\n %d \r\n",alumno1.nombre,alumno1.apellido,alumno1.edad);

	//Defino un puntero a alumno2 y cargo sus datos
	Alumno alumno2;
	Alumno *al = &alumno2;

	strcpy(al->nombre, "Alejandro");
	strcpy(al->apellido, "Genolet");
	al->edad = 21;

	printf("%s \r\n %s \r\n %d \r\n",al->nombre,al->apellido,al->edad);

	return 0;
}

/*==================[end of file]============================================*/


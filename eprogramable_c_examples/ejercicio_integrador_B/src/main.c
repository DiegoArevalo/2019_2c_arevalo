/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/



//Defino struct
typedef struct
{
    char * txt ;
    void (*doit)() ;
} menuEntry ;


//Declaro funciones
void Opcion1();
void Opcion2();
void Opcion3();
void Opcion4();
void EjecutarMenu(menuEntry * menu , int option);




int main(void)
{
	menuEntry menuPrincipal [4] =
	{
	        { "Opción 1" , Opcion1},
			{ "Opción 2" , Opcion2},
			{ "Opción 3" , Opcion3},
			{ "Opción 4" , Opcion4},
	};

	//Defino opción a elegir
	int opcion = 4;

	EjecutarMenu(menuPrincipal, opcion-1);

	return 0;
}

void Opcion1()
{
	printf("Opción 1 \n\r");
}
void Opcion2()
{
	printf("Opción 2 \n\r");
}
void Opcion3()
{
	printf("Opción 3 \n\r");
}
void Opcion4()
{
	printf("Opción 4 \n\r");
}


void EjecutarMenu(menuEntry * menu , int option)
{
	printf("%s \n\r", menu[option].txt);
	menu[option].doit();
}


/*==================[end of file]============================================*/

